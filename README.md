# Imkerei Engel Website

Welcome to the [Imkerei Engel website](https://www.imkerei-engel.de/en) repository! This repository contains the source code for the website of Imkerei Engel, a family beekeeping business in Bad Abbach. The website provides information about the beekeeping process, different honey varieties, and how to get in touch with us.

## About

The Imkerei Engel website showcases the process of beekeeping, from collecting nectar to producing delicious honey. It also highlights different honey varieties that we offer, each with its unique flavor and characteristics.

## Usage and License

The code in this repository is available under the [MIT License](LICENSE). This license allows you to use, modify, and distribute the code for your own projects. However, please note that specific usage restrictions apply to certain files, such as images and videos. Refer to the licensing information provided for each asset.

## Contact

If you have any questions about the Imkerei Engel website, our beekeeping practices, or our honey varieties, feel free to contact us at [imkereiengel@gmail.com](mailto:imkereiengel@gmail.com).

Connect with us on social media:

- [Instagram](https://www.instagram.com/imkerei.engel)
- [Google Maps](https://goo.gl/maps/FyuyfgqtVKKdLwb99)

For legal information and details, please refer to the [Impressum](legal.html) page.

Thank you for your interest in Imkerei Engel!
